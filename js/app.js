var newsTicker = function () {
	//Typing Text (DHTML) v1 (Sunday, April 15th, 2001)
	//Programmed by: Haitham M. Al-Beik
	//Email: albeik26@hotmail.com
	//Visit http://javascriptkit.com for this script

	var ttloop = 1;    // Repeat forever? (1 = True; 0 = False)
	var tspeed = 80;   // Typing speed in milliseconds (larger number = slower)
	var tdelay = 1000; // Time delay between newsTexts in milliseconds

	// ------------- NO EDITING AFTER THIS LINE ------------- \\
	var dwAText, cnews=1, eline=0, cchar=0, mxText;

	function doNews() {
		mxText = document.querySelectorAll('.newsticker-list li').length;
		dwAText = document.querySelector('.newsticker-list li:nth-child('+cnews+')').textContent;
		setTimeout(addChar, 1000);
	}

	function addNews() {
		cnews += 1;
		if (cnews <= mxText) {
			dwAText = document.querySelector('.newsticker-list li:nth-child('+cnews+')').textContent;
			if (dwAText.length != 0) {
				document.querySelector('.newsticker-now').innerHTML = "";
				eline = 0;
				setTimeout(addChar, tspeed)
			}
		}
	}

	function addChar() {
		if (eline!=1) {
			if (cchar != dwAText.length) {
				nmttxt = "";
				for (var k=0; k<=cchar;k++)
					nmttxt += dwAText.charAt(k);
				document.querySelector('.newsticker-now').innerHTML = nmttxt;
				cchar += 1;
				if (cchar != dwAText.length)
					document.querySelector('.newsticker-now').innerHTML += "_";
			} else {
				cchar = 0;
				eline = 1;
			}
			if (mxText==cnews && eline!=0 && ttloop!=0) {
				cnews = 0;
				setTimeout(addNews, tdelay);
			}
			else
				setTimeout(addChar, tspeed);
		} else
			setTimeout(addNews, tdelay)
	}

	doNews();
}

$(function() {
	// newsticker
	newsTicker();
	// relative time using jquery timeago
	$(".timeago").timeago();
});