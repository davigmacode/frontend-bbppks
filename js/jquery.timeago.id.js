// Indonesian
jQuery.timeago.settings.strings = {
  prefixAgo: null,
  prefixFromNow: null,
  suffixAgo: "yang lalu",
  suffixFromNow: "dari sekarang",
  seconds: "kurang dari semenit",
  minute: "satu menit",
  minutes: "%d menit",
  hour: "sejam",
  hours: "%d jam",
  day: "sehari",
  days: "%d hari",
  month: "sebulan",
  months: "%d bulan",
  year: "setahun",
  years: "%d tahun"
};
